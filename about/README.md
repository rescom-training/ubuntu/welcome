# Training overview

* Are you concerned about the vulnerability of your computer to viruses and malicious software? 
* Can your computer hardware keep up with the processes you are running?
* Are you looking for a reliable alternative to avoid system crashes and slowdowns? 
* Do you want to use free and open source software?

Ubuntu is a free and open source Linux distribution. The Linux operating system runs on any hardware, is highly secure, very stable and is not prone to crashes.

This training will provide attendees with their own virtual machine. We provide a gentle introduction to basic Ubuntu usage, including logging into your virtual machine, basic commands in the terminal, installing software on Ubuntu, and transferring data to and from your virtual machine.

## Learning objectives

This training aims to enable students to:

* [ ] familiarise themselves with the Ubuntu desktop and find applications such as Git, LaTeX, MATLAB, Python, RStudio and a text editor;
* [ ] use basic terminal commands to navigate directories (folders) and files;
* [ ] install software using terminal commands;
* [ ] access data on their provided virtual machine.

## Learning outcomes

* Analytical skills: the ability to construct and express logical arguments and to work in abstract or general terms to increase the clarity and efficiency of analysis;
* Computational skills: the ability to adapt existing computational methods and tools to complete a target task;
* Problem solving skills: the ability to engage with unfamiliar problems and identify relevant solution strategies.

---

Continue to [About the trainer](trainer.md), or (re)visit one of the other pages listed [here](../SUMMARY.md).

